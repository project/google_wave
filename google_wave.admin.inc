<?php
/**
 * @file
 * The administration section for the google wave module.
 *
 * @author Jens Jahnke <jan0sch@gmx.net>
 */

/**
 * Generates the form for the admin settings.
 *
 * @param array $form_state
 * @return array
 */
function google_wave_admin_form($form_state) {
  $form['general'] = array(
    '#title' => t('General settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['general']['google_wave_height_firefox_only'] = array(
    '#title' => t('Enable firefox bug workaround'),
    '#description' => t('If you experience ludicrously small embedded waves in firefox enable this option. <em>This workaround simply sets the height of the embedded wave to %d pixels.</em>', array('%d' => (int)variable_get('google_wave_height', GOOGLE_WAVE_HEIGHT))),
    '#type' => 'checkbox',
    '#default_value' => (boolean)variable_get('google_wave_height_firefox_only', GOOGLE_WAVE_HEIGHT_FIREFOX_ONLY),
  );
  $form['teaser'] = array(
    '#title' => t('Teaser settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['teaser']['google_wave_teasers_enabled'] = array(
    '#title' => t('Enable teasers'),
    '#type' => 'checkbox',
    '#default_value' => (int)variable_get('google_wave_teasers_enabled', GOOGLE_WAVE_TEASERS_ENABLED),
  );
  if ((int)variable_get('google_wave_teasers_enabled', GOOGLE_WAVE_TEASERS_ENABLED) === 1) {
    $form['teaser']['google_wave_teaser_height'] = array(
      '#title' => t('Height of a wave teaser'),
      '#description' => t('Enter the height in pixels that the wave teaser should have. If unsure stick with the default value. In case you enter a value below <em>1</em> here the default value (@d) will be used.', array('@d' => GOOGLE_WAVE_TEASER_HEIGHT)),
      '#type' => 'textfield',
      '#default_value' => (int)variable_get('google_wave_teasers_height', GOOGLE_WAVE_TEASER_HEIGHT),
    );
  }
  $form['custom'] = array(
    '#title' => t('Customization'),
    '#description' => t('Please refer to the official documentation for further details: !l', array('!l' => l('http://code.google.com/apis/wave/embed/reference.html', 'http://code.google.com/apis/wave/embed/reference.html'))),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
//  $form['custom']['google_wave_set_size'] = array(
//    '#title' => t('Set size of the embedded wave'),
//    '#description' => t('Enable this option if you want to set a specific size on all embedded waves.'),
//    '#type' => 'checkbox',
//    '#default_value' => (boolean)variable_get('google_wave_set_size', FALSE),
//  );
//  if ((boolean)variable_get('google_wave_set_size', FALSE)) {
//    $form['custom']['google_wave_height'] = array(
//      '#title' => t('Height of the wave element'),
//      '#description' => t('Enter a height in pixels that should be enforced on the wave elements. If you enter a value below <em>1</em> the default value (@d) will be used.', array('@d' => GOOGLE_WAVE_HEIGHT)),
//      '#type' => 'textfield',
//      '#default_value' => (int)variable_get('google_wave_height', GOOGLE_WAVE_HEIGHT),
//    );
//    $form['custom']['google_wave_width'] = array(
//      '#title' => t('Height of the wave element'),
//      '#description' => t('Enter a width in pixels that should be enforced on the wave elements. If you enter a value below <em>1</em> the default value (@d) will be used.', array('@d' => GOOGLE_WAVE_WIDTH)),
//      '#type' => 'textfield',
//      '#default_value' => (int)variable_get('google_wave_width', GOOGLE_WAVE_WIDTH),
//    );
//  }
  if (module_exists('colorpicker')) {
    $form['custom']['google_wave_bgcolor'] = array(
      '#title' => t('Background colour'),
      '#description' => t('Enter the desired background colour of the embedded wave.'),
      '#type' => 'colorpicker_textfield',
      '#default_value' => (string)variable_get('google_wave_bgcolor', ''),
    );
  }
  else {
    $form['custom']['google_wave_bgcolor'] = array(
      '#title' => t('Background colour'),
      '#description' => t('Enter the desired background colour of the embedded wave in hex notation e.g. <code>#00ffaa</code>.'),
      '#type' => 'textfield',
      '#default_value' => (string)variable_get('google_wave_bgcolor', ''),
    );
  }
  $form['custom']['google_wave_footer'] = array(
    '#title' => t('Show footer'),
    '#type' => 'checkbox',
    '#default_value' => (boolean)variable_get('google_wave_footer', FALSE),
  );
  $form['custom']['google_wave_header'] = array(
    '#title' => t('Show header'),
    '#type' => 'checkbox',
    '#default_value' => (boolean)variable_get('google_wave_header', FALSE),
  );
  if ((boolean)variable_get('google_wave_header', FALSE)) {
    $form['custom']['google_wave_toolbar'] = array(
      '#title' => t('Embed toolbar into header'),
      '#type' => 'checkbox',
      '#default_value' => (boolean)variable_get('google_wave_toolbar', FALSE),
    );
  }
  return system_settings_form($form);
} // function google_wave_admin_form

/**
 * Validation function for google_wave_admin_form.
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function google_wave_admin_form_validate($form, &$form_state) {
  if (isset($form_state['values']['google_wave_bgcolor'])) {
    if (!preg_match('/^#[0-9a-fA-F]{6}$/i', $form_state['values']['google_wave_bgcolor'])) {
      form_set_error('google_wave_bgcolor', t('Please enter a valid hexadecimal color code.'));
    }
  }
} // function google_wave_admin_form_validate